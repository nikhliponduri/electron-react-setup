const { app, BrowserWindow } = require('electron');
const path = require('path');

require('electron-reload')(__dirname, {
    electron: path.join(__dirname, 'node_modules', '.bin', 'electron'),
});

function createWindow() {
    // Create the browser window.
    let win = new BrowserWindow({
        width: 800,
        height: 601,
        webPreferences: {
            nodeIntegration: true
        }
    })

    // and load the index.html of the app.
    win.loadFile('index.html');

    const { default: installExtension, REACT_DEVELOPER_TOOLS,REDUX_DEVTOOLS } = require('electron-devtools-installer');
    installExtension(REACT_DEVELOPER_TOOLS).then((name)=>{
        console.log('Added extension',name)
    });
    installExtension(REDUX_DEVTOOLS).then((name)=>{
        console.log('Added extension',name)
    });
}

app.on('ready', createWindow)