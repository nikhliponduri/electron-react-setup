const intialState = {
    profile: undefined,
    carousals: undefined
}

const SET_ADMIN_PROFILE = 'SET_ADMIN_PROFILE';
const SET_ADMIN_CAROUSALS = 'SET_ADMIN_CAROUSALS';
const LOGIN_SUCESS = 'LOGGIN_SUCCESS';
const LOGOUT_SUCESS = 'LOGOUT_SUCESS';

export const setAdminProfile = (profile) => ({
    type: SET_ADMIN_PROFILE,
    profile
});

export const setAdminCarousals = (carousals) => ({
    type: SET_ADMIN_CAROUSALS,
    carousals
});

const adminReducer = (admin = intialState, action) => {
    switch (action.type) {
        case LOGIN_SUCESS:
            return {
                ...admin,
                loggedIn: true
            }
        case LOGOUT_SUCESS:
            return {
                ...intialState
            }
        case SET_ADMIN_CAROUSALS:
            return {
                ...admin,
                carousals: action.carousals
            }
        case SET_ADMIN_PROFILE:
            return {
                ...admin,
                profile: action.profile
            }
        default:
            return admin
    }
}

export default adminReducer;