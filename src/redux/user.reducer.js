const intialState = {
    likedProducts: undefined,
    cart: undefined
}

const SET_USER_LIKED_PRODUCTS = 'SET_USER_LIKED_PRODUCTS';
const LOGIN_SUCESS = 'LOGGIN_SUCCESS';
const LOGOUT_SUCESS = 'LOGOUT_SUCESS';
const SET_USER_CART = 'SET_USER_CART';


export const setUserLikedProducts = (likedProducts) => ({
    type: SET_USER_LIKED_PRODUCTS,
    likedProducts
});

export const setUserCart = (cart) => ({
    type: SET_USER_CART,
    cart
});

const userReducer = (user = intialState, action) => {
    switch (action.type) {
        case LOGIN_SUCESS:
            return {
                ...user,
                loggedIn: true
            }
        case LOGOUT_SUCESS:
            return {
                ...intialState
            }
        case SET_USER_LIKED_PRODUCTS:
            return {
                ...user,
                likedProducts: action.likedProducts
            }
        case SET_USER_CART:
            return {
                ...user,
                cart: action.cart
            }
        default:
            return user
    }
}

export default userReducer;