const intialState = {
    allProducts: {
        data: undefined
    },
    carousals: undefined,
    promos: undefined,
    latestProducts: undefined,
    productLens: undefined,
    selectedProduct: undefined,
    featuredProducts: undefined
}

const SET_ALL_PRODUCTS = 'SET_ALL_PRODUCTS';
const SET_CAROUSALS = 'SET_CAROUSALS';
const SET_PROMOS = 'SET_PROMOS';
const SET_LATEST_PRODUCTS = 'SET_LATEST_PRODUCTS';
const SET_PRODUCT_LENS = 'SET_PRODUCT_LENS';
const SET_SELECTED_PRODUCT = 'SET_SELECTED_PRODUCT';
const SET_FEATURED_PRODUCTS = 'SET_FEATURED_PRODUCTS';

export const setAllProducts = (allProducts) => ({
    type: SET_ALL_PRODUCTS,
    allProducts
});

export const setCarousals = (carousals) => ({
    type: SET_CAROUSALS,
    carousals
});

export const setPromos = (promos) => ({
    type: SET_PROMOS,
    promos
});

export const setLatestProducts = (latestProducts) => ({
    type: SET_LATEST_PRODUCTS,
    latestProducts
})

export const setProductLens = (productLens) => ({
    type: SET_PRODUCT_LENS,
    productLens
})

export const setSelectedProduct = (selectedProduct) => ({
    type: SET_SELECTED_PRODUCT,
    selectedProduct
});

export const setFeaturedProducts = (featuredProducts) => ({
    type: SET_FEATURED_PRODUCTS,
    featuredProducts
})

export default (common = intialState, action) => {
    switch (action.type) {
        case SET_ALL_PRODUCTS:
            return {
                ...common,
                allProducts: action.allProducts
            }
        case SET_CAROUSALS:
            return {
                ...common,
                carousals: action.carousals
            }
        case SET_PROMOS:
            return {
                ...common,
                promos: action.promos
            }
        case SET_LATEST_PRODUCTS:
            return {
                ...common,
                latestProducts: action.latestProducts
            }
        case SET_PRODUCT_LENS:
            return {
                ...common,
                productLens: action.productLens
            }
        case SET_SELECTED_PRODUCT:
            return {
                ...common,
                selectedProduct: action.selectedProduct
            }
        case SET_FEATURED_PRODUCTS:
            return {
                ...common,
                featuredProducts: action.featuredProducts
            }
        default:
            return common
    }
}